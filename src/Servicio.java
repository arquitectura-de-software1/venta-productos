
public class Servicio extends Producto{
	String nombre;
	int precio;
	int cantServicios;
	Servicio(int cant,int precio){
		this.precio = precio;
		this.cantServicios = cant;
	}
	@Override
	boolean estaDisponible(int cant) {
		//Se usa una constante para las pruebas.
		if (20>=cant)
			return true;
		return false;
	}

	@Override
	int calcularTarifa() {
		if(estaDisponible(cantServicios))
		 return cantServicios*precio;
		return 0;
	}

}
