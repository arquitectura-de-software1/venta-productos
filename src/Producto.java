
public abstract class Producto {
  abstract boolean estaDisponible(int cant);
  abstract int calcularTarifa();
}