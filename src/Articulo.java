
public class Articulo extends Producto{
	String nombre;
	int saldo;
	int precio;
	int cantArticulos;
	Articulo(int cant, int precio, int saldo){
		this.cantArticulos = cant;
		this.precio = precio;
		this.saldo = saldo;
	}
	@Override
	boolean estaDisponible(int cant) {
		if (20>=cant)
			return true;
		return false;
	}
	@Override
	int calcularTarifa() {
		if(estaDisponible(cantArticulos))
			 return precio*cantArticulos - saldo*cantArticulos;
		return 0;
	}

}
