import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ProductoTest {
	//SERVICIO
	@Test
	void Deberia_devolver_9_con_cantidad_3_y_precio_3_cuando_CalcularTarifaServicio_() {
		Producto prod1 = new Servicio(3,3);
		assertEquals(9,prod1.calcularTarifa());
	}
	
	@Test
	void Deberia_devolver_9_con_cantidad_4_y_precio_3_cuando_CalcularTarifaServicio_() {
		Producto prod1 = new Servicio(4,3);
		assertEquals(12,prod1.calcularTarifa());
	}
	@Test
	void Deberia_devolver_0_con_cantidad_30_y_precio_4_cuando_CalcularTarifaServicio_() {
		Producto prod1 = new Servicio(30,4);
		assertEquals(0,prod1.calcularTarifa());
	}
	//ARTICULO
	@Test
	void Deberia_devolver_5_con_cantidad_5_precio_3_y_saldo_2_cuando_CalcularTarifaArticulo() {
		Producto prod1 = new Articulo(5,3,2);
		assertEquals(5,prod1.calcularTarifa());
	}
	@Test
	void Deberia_devolver_10_con_cantidad_10_precio_3_y_saldo_2_cuando_CalcularTarifaArticulo() {
		Producto prod1 = new Articulo(10,3,2);
		assertEquals(10,prod1.calcularTarifa());
	}
	@Test
	void Deberia_devolver_0_con_cantidad_50_precio_1_y_saldo_5_cuando_CalcularTarifaArticulo() {
		Producto prod1 = new Articulo(50,1,5);
		assertEquals(0,prod1.calcularTarifa());
	}
	
	//LISTA DE PRODUCTOS
	
	@Test
	void Deberia_devolver_17_con_una_lista_de_productos_al_CalcularTarifca() {
		Venta lista = new Venta();
		Producto prod1 = new Articulo(5,3,2);
		Producto prod2 = new Servicio(4,3);
		Producto prod3 = new Articulo(50,1,5);
			
		lista.agregar(prod1);
		lista.agregar(prod2);
		lista.agregar(prod3);
		assertEquals(17,lista.calcularTarifa());
			
	}	
	@Test
	void Deberia_devolver_27_con_una_lista_de_productos_al_CalcularTarifca() {
		Venta lista = new Venta();
		Producto prod1 = new Articulo(5,3,2);
		Producto prod2 = new Articulo(10,3,2);
		Producto prod3 = new Servicio(4,3);
			
		lista.agregar(prod1);
		lista.agregar(prod2);
		lista.agregar(prod3);
		assertEquals(27,lista.calcularTarifa());
			
	}	

	@Test
	void Deberia_devolver_0_con_una_lista_de_productos_al_CalcularTarifca() {
		Venta lista = new Venta();
		Producto prod1 = new Articulo(60,3,2);
		Producto prod2 = new Articulo(82,3,2);
		Producto prod3 = new Servicio(60,3);
			
		lista.agregar(prod1);
		lista.agregar(prod2);
		lista.agregar(prod3);
		assertEquals(0,lista.calcularTarifa());
			
	}	
}
