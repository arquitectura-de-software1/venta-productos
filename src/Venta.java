import java.util.ArrayList;

public class Venta {
	ArrayList<Producto> productos =new ArrayList<Producto>();  
	
	public void agregar(Producto producto_nuevo)
	{
		productos.add(producto_nuevo);
	}
	
	public int calcularTarifa()
	{
		int total = 0;
		for (Producto producto : productos) {
			total += producto.calcularTarifa();
		}
		return total;
	}
}
